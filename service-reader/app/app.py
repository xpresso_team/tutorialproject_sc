"""
This is a service-reader hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"

import json
from json import JSONDecodeError
import logging
from flask import Flask, Response, render_template, request
import sys
import os
import time
from scripts.SampleService import SampleService

config_file = 'config/dev.json'


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__,template_folder= os.path.join(os.getcwd(),"./templates/"),static_folder=os.path.join(os.getcwd(),".  /templates/static/") )
    return flask_app


app = create_app()

@app.route('/home')
def homepage():
    return render_template('default.html',IP = "/")

@app.route('/result',methods = ["POST"])
def get_results():

    request_body = request.get_json(force=True)
    if "start" not in request_body or "end" not in request_body:
        return Response(status=404,response="Required Parameter not found")

    start = request_body["start"]
    end = request_body["end"]

    service = SampleService()
    results = service.retrieve(start,end)

    resp =  Response(status=200,response= json.dumps({"result":results}))
    resp.headers['Access-Control-Allow-Origin'] = "*"
    resp.headers['Content-Type'] = "application/json"
    return resp

if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
